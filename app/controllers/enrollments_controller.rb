class EnrollmentsController < ApplicationController
  include Databasedotcom::Rails::Controller

  def index
    query = "Select Id, Course__r.Name, Course__r.Description__c, Course__r.Teacher__r.Name From Enrollment__c where Student__c = '#{@contact['Id']}'"
    @enrollments = @client.query(query)
    #@enrollments = Enrollment__c.find_all_by_Student__c(@contact['Id'])
  end

  def show
    enroll_query = "SELECT Id, Name, Student__c, Course__c, Course__r.Name, Course__r.Description__c, (Select Id, Score__c, Completed__c From Enrollment_Exams__r), (SELECT Id, Assignement__r.Description__c FROM Student_Assignements__r) FROM Enrollment__c WHERE Id='#{params[:id]}'"
    @enrollment = @client.query(enroll_query).first
    lessons_query = "SELECT Id, Name__c, Additional_resources__c, (Select Id, Name From Attachments) FROM Lesson__c WHERE Courses__c= '#{@enrollment.Course__c}'"
    @lessons = @client.query(lessons_query)
    @attachment = Attachment.new
  end

  def create
    enrolled_query_str = "Select Id From Enrollment__c Where Course__c='#{enrollment_params[:Course__c]}' And Student__c='#{@contact['Id']}'"
    @enrollment = @client.query(enrolled_query_str).first
    if @enrollment && @enrollment.Id
      redirect_to action: :show, id: @enrollment.Id
    else
      @enrollment = Enrollment__c.new(enrollment_params)
      if @enrollment.save
        redirect_to action: :show, id: @enrollment.Id
      else
        redirect_to action: :show, controller: 'courses', id: enrollment_params[:Course__c], notice: 'Something went wrong!'
      end
    end
  end

  #query = "Select Id, Name, Aditional_resources__c, (Select Id, Body, Name From Attachments) From Lesson__c where Id = '#{@lesson['Id']}'"

  def enrollment_params
    params.require(:enrollment__c).permit(:Course__c).merge(Student__c: @contact['Id'])
  end

end