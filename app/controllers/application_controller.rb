class ApplicationController < ActionController::Base
  include Databasedotcom::Rails::Controller
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :client, :set_contact, :redirect_if_unauthorized


  def index
  end

  private

  def redirect_if_unauthorized
    anon = %w|sessions users|.include?(params[:controller])
    logout = params[:action].eql?('destroy')
    if session[:contact]
      redirect_to root_path if anon && !logout
    else
      redirect_to login_path unless anon
    end
  end

  def client
    @client = dbdc_client
  end

  def set_contact
    @contact = session[:contact] if session[:contact]
  end
end
