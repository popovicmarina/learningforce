class UsersController < ApplicationController
  include Databasedotcom::Rails::Controller
  before_filter :load_user, :except => [:index, :new]
  layout 'anon', only: [:new]

  def index
    @users = Course__c.all
    render json: @users
  end

  def show
  end

  def new
    @user = Contact.new
  end

  def create
    p "paraaaaaa======== #{params[:Birthdate]}"
    Contact.create(contact_params)
    flash[:info] = "The user was created!"
    redirect_to root_path
  end

  def edit
  end

  def update
    @user.update_attributes User.coerce_params(params[:user])
    flash[:info] = "The user was updated!"
    redirect_to user_path(@user)
  end

  def destroy
    @user.delete
    flash[:info] = "The user was deleted!"
    redirect_to users_path
  end

  private

  def load_user
    @user = User.find(params[:id])
  end


  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:FirstName, :LastName, :Email, :Phone, :Password__c, :Description, :Birthplace__c,
                                    :Salutation, :Student__c).merge(Birthdate: "#{params[:contact]['Birthdate(1i)']}-#{params[:contact]['Birthdate(2i)']}-#{params[:contact]['Birthdate(3i)']}")
  end
end