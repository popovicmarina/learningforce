class CoursesController < ApplicationController
  include Databasedotcom::Rails::Controller

  def index
    @courses = Course__c.all
  end

  def show
    enrolled_query_str = "Select Id From Enrollment__c Where Course__c='#{params[:id]}' And Student__c='#{@contact['Id']}'"
    @enrolled = @client.query(enrolled_query_str).size > 0
    query_str = "Select Id, Name, Description__c, Teacher__r.Name, (Select Id, Additional_Resources__c, Name, Name__c from Classes__r) from Course__c where Id = '#{params[:id]}'"
    @course = @client.query(query_str).first
    @enrollment = Enrollment__c.new
  end


end