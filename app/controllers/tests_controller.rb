class TestsController < ApplicationController
  include Databasedotcom::Rails::Controller

  def edit
    @enrollment_exam = Enrollment_Exam__c.find(params[:id])
    @exam = Exam__c.find(@enrollment_exam.Exam__c)
    questions = "Select Id, Text__c, (Select Id, Answer__c From Question_Answers__r) From Question__c where Exam__c='#{@exam.Id}'"
    @questions = @client.query(questions)
  end

  def update
    answers = params[:StudentAnswers__r]
    @enrollment_exam = Enrollment_Exam__c.find(params[:id])
    answers.each do |k,v|
      Student_Answer__c.create(Enrollment_Exam__c: params[:id], Question__c: k, Question_Answer__c: v)
    end
    @enrollment_exam.Completed__c = true
    @enrollment_exam.Submitted_Time__c = Time.now
    @enrollment_exam.save
    redirect_to enrollment_path(@enrollment_exam.Enrollment__c)
  end

end