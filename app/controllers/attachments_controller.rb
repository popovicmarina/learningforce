require 'net/http'
class AttachmentsController < ApplicationController
  include Databasedotcom::Rails::Controller

  def show
    attachment = Attachment.query("Id='#{params[:id]}'")[0]
    url = "#{attachment.client.instance_url}#{attachment.Body}"
    token = attachment.client.oauth_token

    url = URI.parse(url)
    req = Net::HTTP::Get.new(url.to_s)
    req['Authorization'] = "Bearer #{token}"
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    res = http.request(req)
    data = res.body
    send_data data, :disposition => 'attachment', :filename=>attachment.Name
  end

  def create
    @attachment = Attachment.new(attachment_params)
    if @attachment.save
      redirect_to action: :show, controller: 'enrollments', id: params[:enrollment_id]
    else
      redirect_to action: :show, controller: 'enrollments', id: params[:enrollment_id], notice: 'Something went wrong!'
    end
  end

  def attachment_params
    params.require(:attachment).permit(:ParentId).merge(Body: Base64::encode64(params[:attachment][:Body].tempfile.read), IsPrivate: false, Name: params[:attachment][:Body].original_filename)
  end
end