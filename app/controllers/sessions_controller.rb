class SessionsController < ApplicationController
  include Databasedotcom::Rails::Controller

  layout 'anon', only: [:new]

  def new
    @user = Contact.new
  end

  def create
    @contact = Contact.find_by_Email_and_Password__c(*user_params.values)
    if @contact
      session[:contact] = @contact
      redirect_to courses_url, notice: 'You are successfully logged in.'
    else
      redirect_to({ action: 'new' }, notice: 'Email or password is not correct.')
    end
  end

  def destroy
    reset_session
    redirect_to :root
  end

  def user_params
    params.require(:contact).permit(:Email, :Password__c)
  end

end